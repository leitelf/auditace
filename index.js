// Carrega pacotes
const express        = require('express');
const bodyParser     = require('body-parser');
const cors           = require('cors');
const expressLayouts = require('express-ejs-layouts');
const session        = require('express-session');
const fs             = require('fs');
const https          = require('https');
const http           = require('http');
const forceSsl       = require('express-force-ssl');
const uuid           = require('uuid/v4');
const routes         = require('./routes');

require('dotenv').config();

const app = express();
if (process.env.APP_HTTPS) app.use(forceSsl);
app.use(express.static(__dirname + '/static/'));
app.use(expressLayouts);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(session({
    secret: process.env.APP_SESS_SECRET || 'secret',
    resave: true,
    saveUninitialized: true,
    cookie: {
        secure: process.env.APP_SESS_SECURE || false, 
        sameSite: true
    },
    genid: (req) => {
        return uuid();
    }
}));
app.set('view engine', 'ejs');
app.set('layout extractScripts', true);
app.set('layout extractStyles', true);
app.set('layout extractMetas', true);
app.use(routes);

// Inicia servidor
const portHttp = process.env.APP_PORT_HTTP || '8080';
const host = process.env.APP_HOST || 'localhost';

if (process.env.APP_HTTPS) {
    const portHttps = process.env.APP_PORT_HTTPS || null;
    const sslOptions = {
        key: fs.readFileSync(process.env.APP_SSL_KEY),
        cert: fs.readFileSync(process.env.APP_SSL_CRT),
    };
    https.createServer(sslOptions, app).listen(portHttps, () => {
        console.log(`Servidor escutando em https://${host}:${portHttps}`);
    });
}

http.createServer(app).listen(portHttp, () => {
    console.log(`Servidor escutando em http://${host}:${portHttp}`);
});
