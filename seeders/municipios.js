'use strict';

// Carrega pacotes
const fs = require('fs');
const path = require('path');
const csvJson = require('../scripts/csvJson');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        console.log('Carregando municipios...');
        
        let filePath = path.join(__dirname, '../data/municipios.csv' );
        let csvFile = fs.readFileSync(filePath, {encoding: 'utf-8'});
        csvFile = csvFile.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        let municipios = csvJson(csvFile, ';');

        console.log('Salvando em banco de dados...');
        
        await queryInterface.bulkInsert('municipios', municipios, {});
        
        console.log('Dados registrados!');
    },

    down: () => {
        return;
    }
};
