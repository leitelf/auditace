'use strict';

// Carrega pacotes
const fs = require('fs');
const path = require('path');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        console.log('Carregando contratados...');

        let dirPath = path.join(__dirname, '../data/contratados');
        let files = fs.readdirSync(dirPath);
        let contratados = [];
        for (let file in files) {
            let contratadosTmp = fs.readFileSync(path.join(dirPath, files[file]));
            contratadosTmp = await JSON.parse(contratadosTmp);
            contratados = contratados.concat(contratadosTmp.contratados);
        }

        console.log('Salvando em banco de dados...');

        await queryInterface.bulkInsert('contratados', contratados, {});

        console.log('Dados registrados!');      
    },

    down: () => {
        return;
    }
};
