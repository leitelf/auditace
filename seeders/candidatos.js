'use strict';

// Carrega pacotes
const fs = require('fs');
const path = require('path');
const csvJson = require('../scripts/csvJson');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        console.log('Carregando candidatos...');
        
        let filePath = path.join(__dirname, '../data/candidatos.csv' );
        let csvFile = fs.readFileSync(filePath, {encoding: 'latin1'});
        csvFile = csvFile.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        let candidatos = csvJson(csvFile, ';');

        for (let candidato in candidatos) {
            candidatos[candidato].valor_receita = parseFloat(
                await candidatos[candidato].valor_receita.replace(',', '.')
            );
        }

        console.log('Salvando em banco de dados...');

        await queryInterface.bulkInsert('candidatos', candidatos, {});
        
        console.log('Dados registrados!');
    },

    down: () => {
        return;
    }
};
