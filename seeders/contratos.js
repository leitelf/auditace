'use strict';

// Carrega pacotes
const fs = require('fs');
const path = require('path');

module.exports = {
    up: async (queryInterface, Sequelize) => {
        console.log('Carregando contratos...');

        let dirPath = path.join(__dirname, '../data/contratos');
        let files = fs.readdirSync(dirPath);
        let contratos = [];
        for (let file in files) {
            let contratosTmp = fs.readFileSync(path.join(dirPath, files[file]));
            contratosTmp = await JSON.parse(contratosTmp).contratos;
            for (let contrato in contratosTmp) {
                contratosTmp[contrato]
                    .valor_total_contrato = parseFloat(
                        contratosTmp[contrato].valor_total_contrato
                    );
                if (contratosTmp[contrato].numero_contrato_original == '' ||
                    contratosTmp[contrato].numero_contrato_original == ' ')
                    contratosTmp[contrato].numero_contrato_original = null;

                if (contratosTmp[contrato].data_fim_vigencia_contrato == '')
                    contratosTmp[contrato].data_fim_vigencia_contrato = null;
                
                if (contratosTmp[contrato].data_inicio_vigencia_contrato == '')
                    contratosTmp[contrato].data_inicio_vigencia_contrato = null;
                
                if (contratosTmp[contrato].data_contrato_original == '')
                    contratosTmp[contrato].data_contrato_original = null;

                if (contratosTmp[contrato].data_contrato == '')
                    contratosTmp[contrato].data_contrato = null;
                
            }
            contratos = contratos.concat(contratosTmp);
        }

        console.log('Salvando em banco de dados...');

        await queryInterface.bulkInsert('contratos', contratos, {});

        console.log('Dados registrados!');        
    },

    down: () => {
        return;
    }
};
