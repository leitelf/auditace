'use strict';

module.exports = (sequelize, DataTypes) => {
    const Empresa = sequelize.define('Empresa', {
        cnpj: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.STRING
        },
        
        identificador_matriz_filial: {
            type: DataTypes.STRING,
            allowNull: true
        },
        razao_social: {
            type: DataTypes.STRING,
            allowNull: true
        },
        nome_fantasia: {
            type: DataTypes.STRING,
            allowNull: true
        },
        situacao_cadastral: {
            type: DataTypes.STRING,
            allowNull: true
        },
        motivo_situacao_cadastral: {
            type: DataTypes.STRING,
            allowNull: true
        },
        nome_cidade_exterior: {
            type: DataTypes.STRING,
            allowNull: true
        },
        codigo_natureza_juridica: {
            type: DataTypes.STRING,
            allowNull: true
        },
        data_inicio_atividade: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cnae_fiscal: {
            type: DataTypes.STRING,
            allowNull: true
        },
        descricao_tipo_logradouro: {
            type: DataTypes.STRING,
            allowNull: true
        },
        logradouro: {
            type: DataTypes.STRING,
            allowNull: true
        },
        numero: {
            type: DataTypes.STRING,
            allowNull: true
        },
        complemento: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bairro: {
            type: DataTypes.STRING,
            allowNull: true
        },
        cep: {
            type: DataTypes.STRING,
            allowNull: true
        },
        uf: {
            type: DataTypes.STRING,
            allowNull: true
        },
        codigo_municipio: {
            type: DataTypes.STRING,
            allowNull: true
        },
        municipio: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ddd_telefone_1: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ddd_telefone_2: {
            type: DataTypes.STRING,
            allowNull: true
        },
        ddd_fax: {
            type: DataTypes.STRING,
            allowNull: true
        },
        qualificacao_do_responsavel: {
            type: DataTypes.STRING,
            allowNull: true
        },
        capital_social: {
            type: DataTypes.STRING,
            allowNull: true
        },
        porte: {
            type: DataTypes.STRING,
            allowNull: true
        },
        opcao_pelo_simples: {
            type: DataTypes.STRING,
            allowNull: true
        },
        data_opcao_pelo_simples: {
            type: DataTypes.DATE,
            allowNull: true
        },
        data_exclusao_do_simples: {
            type: DataTypes.DATE,
            allowNull: true
        },
        opcao_pelo_mei: {
            type: DataTypes.STRING,
            allowNull: true
        },
        situacao_especial: {
            type: DataTypes.STRING,
            allowNull: true
        },
        data_situacao_especial: {
            type: DataTypes.DATE,
            allowNull: true
        },
        data_situacao_cadastral: {
            type: DataTypes.DATE,
            allowNull: true
        }

    }, {
        tableName: 'empresas',
        timestamps: false
    });

    return Empresa;
};
