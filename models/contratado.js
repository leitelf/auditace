'use strict';

module.exports = (sequelize, DataTypes) => {
    const Contratado = sequelize.define('Contratado', {
        codigo_municipio: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cpf_gestor: {
            type: DataTypes.STRING,
            allowNull: false
        },
        data_contrato: {
            type: DataTypes.DATE,
            allowNull: false
        },
        numero_contrato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        numero_documento_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        codigo_tipo_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nome_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        endereco_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        fone_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cep_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nome_municipio_negociante: {
            type: DataTypes.STRING,
            allowNull: false
        },
        codigo_uf: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        tableName: 'contratados',
        timestamps: false
    });

    Contratado.associate = (models) => {
        Contratado.hasMany(models.Candidato);
    };

    return Contratado;
};
