'use strict';

module.exports = (sequelize, DataTypes) => {
    const Contrato = sequelize.define('Contrato', {
        codigo_municipio: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cpf_gestor: {
            type: DataTypes.STRING,
            allowNull: false
        },
        numero_contrato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        tipo_contrato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        modalidade_contrato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        numero_contrato_original: {
            type: DataTypes.STRING,
            allowNull: true
        },
        data_contrato: {
            type: DataTypes.DATE,
            allowNull: false
        },
        data_contrato_original: {
            type: DataTypes.DATE,
            allowNull: true
        },
        data_inicio_vigencia_contrato: {
            type: DataTypes.DATE,
            allowNull: false
        },
        data_fim_vigencia_contrato: {
            type: DataTypes.DATE,
            allowNull: false
        },
        descricao_objeto_contrato: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        valor_total_contrato: {
            type: DataTypes.DECIMAL,
            allowNull: false
        }
    }, {
        tableName: 'contratos',
        timestamps: false
    });

    Contrato.associate = (models) => {
        Contrato.hasOne(models.Contratado);
    };

    return Contrato;
};
