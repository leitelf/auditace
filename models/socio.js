'use strict';

module.exports = (sequelize, DataTypes) => {
    const Socio = sequelize.define('Socio', {
        cnpj: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: 'pk'
        },
        identificador_de_socio: {
            type: DataTypes.STRING,
            allowNull: true
        },
        nome_socio: {
            type: DataTypes.STRING,
            allowNull: true
        },
        cnpj_cpf_do_socio: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: 'pk'

        },
        codigo_qualificacao_socio: {
            type: DataTypes.STRING,
            allowNull: true
        },
        percentual_capital_social: {
            type: DataTypes.STRING,
            allowNull: true
        },
        data_entrada_sociedade: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cpf_representante_legal: {
            type: DataTypes.STRING,
            allowNull: true
        },
        nome_representante_legal: {
            type: DataTypes.STRING,
            allowNull: true
        },
        codigo_qualificacao_representante_legal: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        tableName: 'socios',
        timestamps: false
    });

    return Socio;
}
