'use strict';

module.exports = (sequelize, DataTypes) => {
    const Denuncia = sequelize.define('Denuncia', {
        tipo_item: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        id_item: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        tipo: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        nome: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cpf: {
            type: DataTypes.STRING,
            allowNull: false
        },
        descricao: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        anonimo: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        tableName: 'denuncias',
        timestamps: false
    });

    return Denuncia;
};
