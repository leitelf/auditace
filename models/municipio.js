'use strict';

module.exports = (sequelize, DataTypes) => {
    const Municipio = sequelize.define('Municipio', {
        codigo_municipio: {
            type: DataTypes.STRING,
            unique: true,
            primaryKey: true
        },
        nome_municipio: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        }
    }, {
        tableName: 'municipios',
        timestamps: false
    });

    return Municipio;
};
