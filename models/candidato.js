'use strict';

module.exports = (sequelize, DataTypes) => {
    const Candidato = sequelize.define('Candidato', {
        municipio: {
            type: DataTypes.STRING,
            allowNull: false
        },
        partido: {
            type: DataTypes.STRING,
            allowNull: false
        },
        num_candidato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cargo: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nome_candidato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cpf_candidato: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cpf_cnpj_doador: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nome_doador: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nome_doador_receita_federal: {
            type: DataTypes.STRING,
            allowNull: false
        },
        data_receita: {
            type: DataTypes.DATE,
            allowNull: false
        },
        valor_receita: {
            type: DataTypes.DECIMAL,
            allowNull: false
        },
        tipo_receita: {
            type: DataTypes.STRING,
            allowNull: false
        },
        fonte_recurso: {
            type: DataTypes.STRING,
            allowNull: false
        },
        especie_recurso: {
            type: DataTypes.STRING,
            allowNull: false
        },
        descricao_receita: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        ano_eleicao: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        tableName: 'candidatos',
        timestamps: false
    });

    return Candidato;
};
