'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('socios', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            cnpj: {
                type: Sequelize.STRING,
                allowNull: true
            },
            identificador_de_socio: {
                type: Sequelize.STRING,
                allowNull: true
            },
            nome_socio: {
                type: Sequelize.STRING,
                allowNull: true
            },
            cnpj_cpf_do_socio: {
                type: Sequelize.STRING,
                allowNull: true
    
            },
            codigo_qualificacao_socio: {
                type: Sequelize.STRING,
                allowNull: true
                
            },
            percentual_capital_social: {
                type: Sequelize.STRING,
                allowNull: true
            },
            data_entrada_sociedade: {
                type: Sequelize.DATE,
                allowNull: true
            },
            cpf_representante_legal: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            nome_representante_legal: {
                type: Sequelize.STRING,
                allowNull: true
            },
            codigo_qualificacao_representante_legal: {
                type: Sequelize.STRING,
                allowNull: true,
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('socios');
    }
};
