'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('empresas', {
            cnpj: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.STRING
            },
            
            identificador_matriz_filial: {
                type: Sequelize.STRING,
                allowNull: true
            },
            razao_social: {
                type: Sequelize.STRING,
                allowNull: true
            },
            nome_fantasia: {
                type: Sequelize.STRING,
                allowNull: true
            },
            situacao_cadastral: {
                type: Sequelize.STRING,
                allowNull: true
            },
            motivo_situacao_cadastral: {
                type: Sequelize.STRING,
                allowNull: true
            },
            nome_cidade_exterior: {
                type: Sequelize.STRING,
                allowNull: true
            },
            codigo_natureza_juridica: {
                type: Sequelize.STRING,
                allowNull: true
            },
            data_inicio_atividade: {
                type: Sequelize.DATE,
                allowNull: true
            },
            cnae_fiscal: {
                type: Sequelize.STRING,
                allowNull: true
            },
            descricao_tipo_logradouro: {
                type: Sequelize.STRING,
                allowNull: true
            },
            logradouro: {
                type: Sequelize.STRING,
                allowNull: true
            },
            numero: {
                type: Sequelize.STRING,
                allowNull: true
            },
            complemento: {
                type: Sequelize.STRING,
                allowNull: true
            },
            bairro: {
                type: Sequelize.STRING,
                allowNull: true
            },
            cep: {
                type: Sequelize.STRING,
                allowNull: true
            },
            uf: {
                type: Sequelize.STRING,
                allowNull: true
            },
            codigo_municipio: {
                type: Sequelize.STRING,
                allowNull: true
            },
            municipio: {
                type: Sequelize.STRING,
                allowNull: true
            },
            ddd_telefone_1: {
                type: Sequelize.STRING,
                allowNull: true
            },
            ddd_telefone_2: {
                type: Sequelize.STRING,
                allowNull: true
            },
            ddd_fax: {
                type: Sequelize.STRING,
                allowNull: true
            },
            qualificacao_do_responsavel: {
                type: Sequelize.STRING,
                allowNull: true
            },
            capital_social: {
                type: Sequelize.STRING,
                allowNull: true
            },
            porte: {
                type: Sequelize.STRING,
                allowNull: true
            },
            opcao_pelo_simples: {
                type: Sequelize.STRING,
                allowNull: true
            },
            data_opcao_pelo_simples: {
                type: Sequelize.DATE,
                allowNull: true
            },
            data_exclusao_do_simples: {
                type: Sequelize.DATE,
                allowNull: true
            },
            opcao_pelo_mei: {
                type: Sequelize.STRING,
                allowNull: true
            },
            situacao_especial: {
                type: Sequelize.STRING,
                allowNull: true
            },
            data_situacao_especial: {
                type: Sequelize.DATE,
                allowNull: true
            },
            data_situacao_cadastral: {
                type: Sequelize.DATE,
                allowNull: true
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('empresas');
    }
};
