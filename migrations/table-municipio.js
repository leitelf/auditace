'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('municipios', {
            codigo_municipio: {
                type: Sequelize.STRING,
                unique: true,
                primaryKey: true
            },
            nome_municipio: {
                type: Sequelize.STRING,
                unique: true,
                allowNull: false
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('municipios');
    }
};
