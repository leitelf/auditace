'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('contratados', {
            id: {
                type: Sequelize.INTEGER,
                unique: true,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            codigo_municipio: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cpf_gestor: {
                type: Sequelize.STRING,
                allowNull: false
            },
            data_contrato: {
                type: Sequelize.DATE,
                allowNull: false
            },
            numero_contrato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            numero_documento_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            codigo_tipo_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            nome_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            endereco_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            fone_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cep_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            nome_municipio_negociante: {
                type: Sequelize.STRING,
                allowNull: false
            },
            codigo_uf: {
                type: Sequelize.STRING,
                allowNull: false
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('contratados');
    }
};
