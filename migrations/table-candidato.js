'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('candidatos', {
            id: {
                type: Sequelize.INTEGER,
                unique: true,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            municipio: {
                type: Sequelize.STRING,
                allowNull: false
            },
            partido: {
                type: Sequelize.STRING,
                allowNull: false
            },
            num_candidato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cargo: {
                type: Sequelize.STRING,
                allowNull: false
            },
            nome_candidato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cpf_candidato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cpf_cnpj_doador: {
                type: Sequelize.STRING,
                allowNull: false
            },
            nome_doador: {
                type: Sequelize.STRING,
                allowNull: false
            },
            nome_doador_receita_federal: {
                type: Sequelize.STRING,
                allowNull: false
            },
            data_receita: {
                type: Sequelize.DATE,
                allowNull: false
            },
            valor_receita: {
                type: Sequelize.DECIMAL,
                allowNull: false
            },
            tipo_receita: {
                type: Sequelize.STRING,
                allowNull: false
            },
            fonte_recurso: {
                type: Sequelize.STRING,
                allowNull: false
            },
            especie_recurso: {
                type: Sequelize.STRING,
                allowNull: false
            },
            descricao_receita: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            ano_eleicao: {
                type: Sequelize.STRING,
                allowNull: false
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('candidatos');
    }
};
