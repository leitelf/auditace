'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('denuncias', {
            id: {
                type: Sequelize.INTEGER,
                unique: true,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            tipo_item: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            id_item: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            tipo: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            nome: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cpf: {
                type: Sequelize.STRING,
                allowNull: false
            },
            descricao: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            anonimo: {
                type: Sequelize.BOOLEAN,
                allowNull: false
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('denuncias');
    }
};
