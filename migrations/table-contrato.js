'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('contratos', {
            id: {
                type: Sequelize.INTEGER,
                unique: true,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            codigo_municipio: {
                type: Sequelize.STRING,
                allowNull: false
            },
            cpf_gestor: {
                type: Sequelize.STRING,
                allowNull: false
            },
            numero_contrato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            tipo_contrato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            modalidade_contrato: {
                type: Sequelize.STRING,
                allowNull: false
            },
            numero_contrato_original: {
                type: Sequelize.STRING,
                allowNull: true
            },
            data_contrato: {
                type: Sequelize.DATE,
                allowNull: false
            },
            data_contrato_original: {
                type: Sequelize.DATE,
                allowNull: true
            },
            data_inicio_vigencia_contrato: {
                type: Sequelize.DATE,
                allowNull: false
            },
            data_fim_vigencia_contrato: {
                type: Sequelize.DATE,
                allowNull: false
            },
            descricao_objeto_contrato: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            valor_total_contrato: {
                type: Sequelize.DECIMAL,
                allowNull: false
            }
        });
    },
    
    down: (queryInterface) => {
        return queryInterface.dropTable('contratos');
    }
};
