'use strict';

const Denuncia = require('../models').Denuncia;

module.exports = {
    save: async (req, res) => {
        let obj = {
            nome: req.body.nome,
            cpf: req.body.cpf,
            tipo: req.body.tipo,
            descricao: req.body.descricao,
            anonimo: req.body.anonimo ? true : false,
            tipo_item: req.body.tipo_item,
            id_item: req.body.id_item
        }

        try {
            await Denuncia.create(obj);
            res.redirect('/fiscalizacao/contrato/' + obj.id_item);
        } catch (err) {
            console.log(err);
            res.end();
        }
        
        
    }
}