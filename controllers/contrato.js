'use strict';

const Contrato = require('../models').Contrato;
const Sequelize = require('sequelize');
const verDistante = require('../scripts/verDistante');
const verValor = require('../scripts/verValor');
const verFrequente = require('../scripts/verFrequente');
const verFinanciador = require('../scripts/verFinanciador');
const verRecente = require('../scripts/verRecente');
const Denuncia = require('../models').Denuncia;
const Municipio = require('../models').Municipio;

require('dotenv').config();

let queryAll = (codigo_municipio, data_inicio, 
data_fim, valor_min, valor_max, fornecedor) => {
        return 'SELECT DISTINCT "id", * \
        FROM (SELECT "Contrato"."id", \
        "Contrato"."codigo_municipio", "Contrato"."cpf_gestor", \
        "Contrato"."numero_contrato", "Contrato"."tipo_contrato", \
        "Contrato"."modalidade_contrato", \
        "Contrato"."numero_contrato_original", \
        "Contrato"."data_contrato", "Contrato"."data_contrato_original", \
        "Contrato"."data_inicio_vigencia_contrato", \
        "Contrato"."data_fim_vigencia_contrato", \
        "Contrato"."descricao_objeto_contrato", \
        "Contrato"."valor_total_contrato", \
        "Contratado"."numero_documento_negociante", \
        "Contratado"."codigo_tipo_negociante", \
        "Contratado"."nome_negociante", \
        "Contratado"."endereco_negociante", \
        "Contratado"."fone_negociante", \
        "Contratado"."cep_negociante", \
        "Contratado"."nome_municipio_negociante", \
        "Contratado"."codigo_uf", \
        "Candidato"."partido", \
        "Candidato"."num_candidato", \
        "Candidato"."cargo", \
        "Candidato"."nome_candidato", \
        "Candidato"."cpf_candidato", \
        "Candidato"."nome_doador", \
        "Candidato"."nome_doador_receita_federal", \
        "Candidato"."data_receita", \
        "Candidato"."valor_receita", \
        "Candidato"."tipo_receita", \
        "Candidato"."fonte_recurso", \
        "Candidato"."especie_recurso", \
        "Candidato"."descricao_receita", \
        "Candidato"."ano_eleicao", \
        "Candidato"."municipio", \
        "Empresa"."cnpj", "Empresa"."data_inicio_atividade", \
        "Empresa"."descricao_tipo_logradouro", "Empresa"."logradouro", \
        "Empresa"."numero", "Empresa"."complemento", "Empresa"."bairro", \
        "Empresa"."cep", "Empresa"."uf", "Empresa"."razao_social" \
        FROM "contratos" AS "Contrato" LEFT OUTER JOIN "contratados" AS \
        "Contratado" ON \
        "Contratado"."codigo_municipio" = "Contrato"."codigo_municipio" \
        AND "Contratado"."data_contrato" = "Contrato"."data_contrato" \
        AND "Contratado"."numero_contrato" = "Contrato"."numero_contrato" \
        AND "Contratado"."cpf_gestor" = "Contrato"."cpf_gestor" \
        LEFT OUTER JOIN "candidatos" AS "Candidato" ON \
        "Candidato"."cpf_cnpj_doador" = \
        "Contratado"."numero_documento_negociante" \
        LEFT OUTER JOIN "empresas" AS "Empresa" ON "Empresa"."cnpj" = \
        "Contratado"."numero_documento_negociante"  WHERE \
        ("Contrato"."codigo_municipio" = \'' + codigo_municipio + '\' \
        AND "Contrato"."data_contrato" >= \'' + data_inicio + '\' \
        AND "Contrato"."data_contrato" <= \'' + data_fim + '\' \
        AND "Contrato"."valor_total_contrato" >= ' + valor_min + ' \
        AND "Contrato"."valor_total_contrato" <= ' + valor_max + ' \
        AND "Contratado"."nome_negociante" LIKE \'%' + fornecedor + '%\')) T';
};

let queryOne = (id) => {
    return 'SELECT "Contrato"."id", \
    "Contrato"."codigo_municipio", "Contrato"."cpf_gestor", \
    "Contrato"."numero_contrato", "Contrato"."tipo_contrato", \
    "Contrato"."modalidade_contrato", \
    "Contrato"."numero_contrato_original", \
    "Contrato"."data_contrato", "Contrato"."data_contrato_original", \
    "Contrato"."data_inicio_vigencia_contrato", \
    "Contrato"."data_fim_vigencia_contrato", \
    "Contrato"."descricao_objeto_contrato", \
    "Contrato"."valor_total_contrato", \
    "Contratado"."numero_documento_negociante", \
    "Contratado"."codigo_tipo_negociante", \
    "Contratado"."nome_negociante", \
    "Contratado"."endereco_negociante", \
    "Contratado"."fone_negociante", \
    "Contratado"."cep_negociante", \
    "Contratado"."nome_municipio_negociante", \
    "Contratado"."codigo_uf", \
    "Candidato"."partido", \
    "Candidato"."num_candidato", \
    "Candidato"."cargo", \
    "Candidato"."nome_candidato", \
    "Candidato"."cpf_candidato", \
    "Candidato"."nome_doador", \
    "Candidato"."nome_doador_receita_federal", \
    "Candidato"."data_receita", \
    "Candidato"."valor_receita", \
    "Candidato"."tipo_receita", \
    "Candidato"."fonte_recurso", \
    "Candidato"."especie_recurso", \
    "Candidato"."descricao_receita", \
    "Candidato"."ano_eleicao", \
    "Candidato"."municipio", \
    "Empresa"."cnpj", "Empresa"."data_inicio_atividade", \
    "Empresa"."descricao_tipo_logradouro", "Empresa"."logradouro", \
    "Empresa"."numero", "Empresa"."complemento", "Empresa"."bairro", \
    "Empresa"."cep", "Empresa"."uf", "Empresa"."razao_social" \
    FROM "contratos" AS "Contrato" LEFT OUTER JOIN "contratados" AS \
    "Contratado" ON \
    "Contratado"."codigo_municipio" = "Contrato"."codigo_municipio" \
    AND "Contratado"."data_contrato" = "Contrato"."data_contrato" \
    AND "Contratado"."numero_contrato" = "Contrato"."numero_contrato" \
    AND "Contratado"."cpf_gestor" = "Contrato"."cpf_gestor" \
    LEFT OUTER JOIN "candidatos" AS "Candidato" ON \
    "Candidato"."cpf_cnpj_doador" = \
    "Contratado"."numero_documento_negociante" \
    LEFT OUTER JOIN "empresas" AS "Empresa" ON "Empresa"."cnpj" = \
    "Contratado"."numero_documento_negociante" WHERE \
    ("Contrato"."id" = \'' + id + '\')';
};

module.exports = {
    queryAll: queryAll,

    queryOne: queryOne,

    getAll: async (req, res) => {
        try {
            let codigo_municipio = req.params.codigo_municipio;
            let data_inicio = new Date(req.params.data_inicio).toISOString();
            let data_fim = new Date(req.params.data_fim).toISOString();
            let valor_max = parseFloat(req.params.valor_max);
            let valor_min = parseFloat(req.params.valor_min);
            let fornecedor = req.params.fornecedor;

            if (typeof fornecedor !== 'undefined' && fornecedor) {
                fornecedor = decodeURI(fornecedor);
            } else {
                fornecedor = '';
            }

            const sequelize = new Sequelize(
                process.env.DB_NAME,
                process.env.DB_USERNAME,
                process.env.DB_PASSWORD,
                {
                    host: process.env.DB_HOST,
                    dialect: process.env.DB_DIALECT,
                    logging: false
                }
            );    

            let municipio = await Municipio.findByPk(codigo_municipio);
            
            let contratos = await sequelize.query(queryAll(codigo_municipio, 
            data_inicio, data_fim, valor_min, valor_max, fornecedor),
            { model: Contrato, mapToModel: true});


            for (let contrato in contratos) {
                contratos[contrato].dataValues
                    .alerta_distante = await verDistante
                        .contrato(contratos[contrato]);

                contratos[contrato].dataValues
                    .alerta_valor = verValor
                        .contrato(contratos[contrato]);

                contratos[contrato].dataValues
                    .alerta_financiador = await verFinanciador
                        .contrato(contratos[contrato], municipio.nome_municipio);
                
                contratos[contrato].dataValues
                    .alerta_recente = await verRecente
                        .contrato(contratos[contrato]);
                
                

                contratos[contrato].dataValues
                    .alerta_denuncias = await Denuncia.count({
                        where: {
                            tipo_item: 0,
                            id_item: contratos[contrato].dataValues.id
                        }
                    });

                contratos[contrato].dataValues
                    .alerta_frequente = await verFrequente
                        .contrato(contratos[contrato]);

                contratos[contrato].dataValues
                    .alerta_nota = contratos[contrato].dataValues.alerta_distante + contratos[contrato].dataValues.alerta_valor * 4 + contratos[contrato].dataValues.alerta_financiador * 8 + contratos[contrato].dataValues.alerta_recente * 5 + contratos[contrato].dataValues.alerta_denuncias * 7 + contratos[contrato].dataValues.alerta_frequente * 2;
            }

            if (contratos.length >= 1) {
                res.json(contratos);
            } else {
                res.status(404).json(null);
            }

        } catch (err) {
            console.log(err);
            res.status(500).end();
        }
    },

    // get: async (req, res) => {
    //     try {
    //         let id = req.params.id;

    //         const sequelize = new Sequelize(
    //             process.env.DB_NAME,
    //             process.env.DB_USERNAME,
    //             process.env.DB_PASSWORD,
    //             {
    //                 host: process.env.DB_HOST,
    //                 dialect: process.env.DB_DIALECT,
    //                 logging: false
    //             }
    //         );

    //         let contrato = await sequelize.query(queryOne(id),
    //         { model: Contrato, mapToModel: true});


    //         if (contrato[0]) {
    //             contrato[0].dataValues.alerta_distante = await verDistante
    //                 .contrato(contrato[0]);

    //             contrato[0].dataValues.alerta_valor = verValor
    //                 .contrato(contrato[0]);

    //             contrato[0].dataValues.alerta_financiador = await verFinanciador
    //                 .contrato(contrato[0]);

    //             contrato[0].dataValues.alerta_recente = await verRecente
    //                     .contrato(contrato[0]);
                
    //             contrato[0].dataValues.alerta_denuncias = await Denuncia
    //                 .count({
    //                     where: {
    //                         tipo_item: 0,
    //                         id_item: contrato[0].dataValues.id
    //                     }
    //                 });

    //                 contrato[0].dataValues
    //                     .alerta_frequente = await verFrequente
    //                         .contrato(contrato[0]);

    //             contrato[0].dataValues
    //                     .alerta_nota = contrato[0].dataValues.alerta_distante + contrato[0].dataValues.alerta_valor + contrato[0].dataValues.alerta_financiador + contrato[0].dataValues.alerta_recente + contrato[0].dataValues.alerta_denuncias + contrato[0].dataValues.alerta_frequente;

    //             res.json(contrato[0]);
    //         } else {
    //             res.status(404).json(null);
    //         }

            

    //     } catch (err) {
    //         console.log(err);
    //         res.status(500).end();
    //     }
    // }
};