'use strict';

const Municipio = require('../models').Municipio;

module.exports = {
    getAll: async (req, res) => {
        try {
            let municipios = await Municipio.findAll({});
            res.json(municipios);
        } catch (err) {
            console.log(err);
            res.status(500).end();
        }
    },

    get: async (req, res) => {
        try {
            let codigo_municipio = req.params.codigo_municipio;
            let municipio = await Municipio.findByPk(codigo_municipio);
            if (municipio) 
                res.json(municipio);
            else
                res.status(404).json(null);
        } catch (err) {
            console.log(err);
            res.status(500).end();
        }
    }
};
