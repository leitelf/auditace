require('dotenv').config();

module.exports = {
    'development': {
        'username': process.env.DB_USERNAME || 'root',
        'password': process.env.DB_PASSWORD || null,
        'database': process.env.DB_NAME || 'database_development',
        'host': process.env.DB_HOST || '127.0.0.1',
        'port': process.env.DB_PORT || '5432',
        'dialect': process.env.DB_DIALECT || 'mysql',
        'operatorsAliases': 0,
        'logging': false
    },
    'test': {
        'username': process.env.DB_USERNAME || 'root',
        'password': process.env.DB_PASSWORD || null,
        'database': process.env.DB_NAME || 'database_development',
        'host': process.env.DB_HOST || '127.0.0.1',
        'port': process.env.DB_PORT || '5432',
        'dialect': process.env.DB_DIALECT || 'mysql',
        'operatorsAliases': 0,
        'logging': false
    },
    'production': {
        'username': process.env.DB_USERNAME || 'root',
        'password': process.env.DB_PASSWORD || null,
        'database': process.env.DB_NAME || 'database_development',
        'host': process.env.DB_HOST || '127.0.0.1',
        'port': process.env.DB_PORT || '5432',
        'dialect': process.env.DB_DIALECT || 'mysql',
        'operatorsAliases': 0,
        'logging': false
    }
};
