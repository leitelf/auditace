'use strict';

// Carrega pacotes
const router = require('express').Router();
const municipioController = require('../controllers/municipio');
const contratoController = require('../controllers/contrato');
const Municipio = require('../models').Municipio;
const Contrato = require('../models').Contrato;
const Sequelize = require('sequelize');
const verDistante = require('../scripts/verDistante');
const verValor = require('../scripts/verValor');
const verFinanciador = require('../scripts/verFinanciador');
const verRecente = require('../scripts/verRecente');
const verFrequente = require('../scripts/verFrequente');
const denunciaController = require('../controllers/denuncia');
const Denuncia = require('../models').Denuncia;


router.get('/api/municipios', municipioController.getAll);
router.get('/api/municipios/:codigo_municipio', municipioController.get);

router.get('/api/contratos/\
:codigo_municipio/:data_inicio/:data_fim/:valor_min/:valor_max/:fornecedor?', 
contratoController.getAll);

// router.get('/api/contratos/:id', contratoController.get);

router.get('/', (req, res) => {
    
    res.render('pages/index');
});

router.get('/fiscalizacao', async (req, res) => {

    try {
        let municipios = await Municipio.findAll({});
        res.render('pages/fiscalizacao', {municipios: municipios});
    } catch (err) {
        console.log(err);
        res.redirect('/500');
    }    
});

router.get('/denuncia/contrato/:id', async (req, res) => {
    try {
        const sequelize = new Sequelize(
            process.env.DB_NAME,
            process.env.DB_USERNAME,
            process.env.DB_PASSWORD,
            {
                host: process.env.DB_HOST,
                dialect: process.env.DB_DIALECT,
                logging: false
            }
        );

        let contrato = await sequelize
            .query(contratoController.queryOne(req.params.id),
        { model: Contrato, mapToModel: true });

        if (contrato[0]) {
            res.render('pages/denuncia', {contrato: contrato[0].dataValues});
        } else {
            res.redirect('/404');
        }
    } catch (err) {
        console.log(err);
        res.redirect('/500');
    }
});

router.post('/api/denuncia/', denunciaController.save);

router.get('/fiscalizacao/contrato/:id', async (req, res) => {
    try {
        const sequelize = new Sequelize(
            process.env.DB_NAME,
            process.env.DB_USERNAME,
            process.env.DB_PASSWORD,
            {
                host: process.env.DB_HOST,
                dialect: process.env.DB_DIALECT,
                logging: false
            }
        );

        let contrato = await sequelize
            .query(contratoController.queryOne(req.params.id),
        { model: Contrato, mapToModel: true });

        let municipio = await Municipio
                .findByPk(contrato[0].codigo_municipio);

        if (contrato[0]) {
            contrato[0].dataValues.alerta_distante = await verDistante
                    .contrato(contrato[0]);

            contrato[0].dataValues.alerta_valor = verValor
                .contrato(contrato[0]);

            contrato[0].dataValues.alerta_financiador = await verFinanciador
                .contrato(contrato[0], municipio.nome_municipio);

            contrato[0].dataValues.alerta_recente = await verRecente
                    .contrato(contrato[0]);

            contrato[0].dataValues.alerta_denuncias = await Denuncia
            .count({
                where: {
                    tipo_item: 0,
                    id_item: contrato[0].dataValues.id
                }
            });

            contrato[0].dataValues.alerta_frequente = await verFrequente
                    .contrato(contrato[0]);


            contrato[0].dataValues
                    .alerta_nota = contrato[0].dataValues.alerta_distante * 1 + contrato[0].dataValues.alerta_valor * 4 + contrato[0].dataValues.alerta_financiador * 8 + contrato[0].dataValues.alerta_recente * 5 + contrato[0].dataValues.alerta_denuncias * 7 + contrato[0].dataValues.alerta_frequente * 2;


            let denuncias = await Denuncia.findAll({
                where: {
                    tipo_item: 0,
                    id_item: contrato[0].dataValues.id
                }
            });

            


            res.render('pages/detalhes', 
                   {municipio: municipio, contrato: contrato[0].dataValues, denuncias: denuncias});
        } else {
            res.redirect('/404');
        }
    } catch (err) {
        console.log(err);
        res.redirect('/500');
    }

});

module.exports = router;
