# AuditaCE

## Como fazer o Deploy

1. Instalar Nodejs e NPM.
2. Preencher arquivo .env com os valores necessários (ver .sample-env);
3. Instalar módulos do node (``npm i``);
4. Criar tabelas (``npx sequelize-cli db:migrate``);
5. Popular tabelas. Os dados utilizados foram os disponibilizados pela API do TCE (contratos, contratados, municipios),
BrasilIO (empresas) e TSE (candidatos). Podem ser utlizados os seeders disponíveis (``npx sequelize-cli db:seed:all``) com os dados disponibilizados no link;
6. Iniciar servidor (``npm start``).


Os dados usados para popular o banco estão disponíveis em: https://drive.google.com/open?id=1r2asJv3rZ9k4F2Qd79vJlKx8nqUMJ__v