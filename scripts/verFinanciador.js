'use strict';

const Contratado = require('../models').Contratado;
const Candidato = require('../models').Candidato;
const { Op } = require('sequelize');

module.exports = {
    contratoDB: async (item) => {
        let contratado = await Contratado.findOne({
            where: {
                codigo_municipio: item.codigo_municipio,
                data_contrato: item.data_contrato,
                numero_contrato: item.numero_contrato,
                cpf_gestor: item.cpf_gestor
            }
        });
        if (contratado) {
            let ano = new Date(contratado.data_contrato).getFullYear();
            let anoInt = parseInt(ano);
            let max = (anoInt + 4).toString();
            let min = (anoInt - 4).toString();

            let candidato = await Candidato.findAll({
                where: {
                    [Op.or]: [
                        {
                            cpf_cnpj_doador: 
                            contratado.numero_documento_negociante,  
                        },
                        {
                            cpf_cnpj_doador: 
                            contratado.numero_documento_negociante + '.0',
                        }
                    ]
                }
            });

            if (candidato && 
                (candidato.ano_eleicao >= min
                && candidato.ano_eleicao <= max)) return true;
            else return false;
        } else {
            return false;
        }
        
    },

    contrato: async (contrato, municipio) => {
        if (contrato &&
            contrato.dataValues.ano_eleicao &&
            contrato.dataValues.data_contrato) {
            let ano = parseInt(new Date(contrato.dataValues.data_contrato)
                .getFullYear());
            
            if (parseInt(contrato.dataValues.ano_eleicao) >= ano-4 &&
                parseInt(contrato.dataValues.ano_eleicao) <= ano+4 &&
                contrato.dataValues.municipio == municipio) return true;
            else return false;
        } else {
            return false;
        }
    }
}