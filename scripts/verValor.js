'use strict';

module.exports = {
    contrato: (contrato) => {
        let data = new Date(contrato.dataValues.data_contrato);
        let dataLei = new Date('2018-07-19');
        let val = parseFloat(contrato.dataValues.valor_total_contrato);
        let minLim;
        let maxLim;

        if (data < dataLei) {
            minLim = [7000.00, 8000.00];
            maxLim = [14000.00, 15000.00];
        } else {
            minLim = [16000.00, 17600.00];
            maxLim = [30000.00, 33000.00];
        }

        if (contrato.dataValues.tipo_contrato == 'E') {
            if ((val >= maxLim[0] && val <= maxLim[1]))
                return true
            else 
                return false   
        } else if ((val >= minLim[0] && val <= minLim[1]))
            return true
        else
            return false
    }
};
