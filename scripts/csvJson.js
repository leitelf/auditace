'use strict';

module.exports = (csv, divisor = ',') => {
    let rows = csv.split('\n');
    let result = [];
    let headers = rows[0].split(divisor);

    for (let i = 1; i < rows.length; i++) {
        let obj = {};
        let currentRow = rows[i].split(divisor);

        for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentRow[j].toString();
        }
        result.push(obj);
    }
    return result;
};