'use strict';

const Contratado = require('../models').Contratado;
const { Op } = require('sequelize');

module.exports = {
    contrato: async (contrato) => {
        let data = new Date(contrato.dataValues.data_contrato);
        let dataMax = data;
        dataMax.setMonth(data.getMonth() + 3);

        let dataMin = data;
        dataMin.setMonth(data.getMonth() - 3);

        data = data.toISOString();
        dataMin = dataMin.toISOString();
        dataMax = dataMax.toISOString();

        try {
            let qty = await Contratado.count({
                where: {
                    numero_documento_negociante: contrato.dataValues.numero_documento_negociante,
                    numero_contrato: {[Op.ne]: contrato.dataValues.numero_contrato},
                    cpf_gestor: contrato.dataValues.cpf_gestor,
                    data_contrato: {[Op.between]: [dataMin, dataMax]}
                }
            });
    
    
            if (qty > 0) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            console.log(err);
            return false;
        }
        
    }
};
