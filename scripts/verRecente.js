'use strict';

module.exports = {
    contrato: (contrato) => {
        let dataContrato = new Date(contrato.dataValues.data_contrato);
        let dataAbertura = new Date(contrato.dataValues.data_inicio_atividade);
        let anoContrato = dataContrato.getFullYear();
        let anosDepoisEleicao =  anoContrato % 4;
        let anoEleicao;

        if (anosDepoisEleicao != 0) {
            anoEleicao = anoContrato - anosDepoisEleicao;
        } else {
            anoEleicao = anoContrato - 4;
        }

        let dataMin = new Date(anoEleicao + '-07-01');

        if (dataAbertura >= dataMin) {
            return true;
        } else {
            return false;
        }
    }
};
