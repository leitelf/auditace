'use strict';

const Contratado = require('../models').Contratado;

module.exports = {
    contratoDB: async (item) => {
        let contratado = await Contratado.findOne({
            where: {
                codigo_municipio: item.codigo_municipio,
                data_contrato: item.data_contrato,
                numero_contrato: item.numero_contrato,
                cpf_gestor: item.cpf_gestor
            }
        });
        if (contratado && contratado.codigo_uf != 'CE') return true;       
        return false;
    },

    contrato: async (contrato) => {
        if (contrato &&
            contrato.dataValues.codigo_uf &&
            contrato.dataValues.codigo_uf != 'CE')
            return true;
        else return false;
    }
};
