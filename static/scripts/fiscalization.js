var listData = [];
var curPage = 0;
var pagesQty = 0;
var searchConf = {};
const pageLimit = 15;

function getCity (city_id) {
    return $('.city-opt[value="'+city_id+'"]').html();
}

function formatDate (dateStr) {
    var date = new Date(dateStr);
    var dd = date.getDate().toString();
    dd = (dd < 10) ? '0' + dd : dd;
    var mm = (date.getMonth() + 1).toString();
    mm = (mm < 10) ? '0' + mm : mm;
    return dd + '/' + mm + '/' + date.getFullYear();
}

function genItem (itemData) {

    let dataIni = new Date (itemData.data_inicio_vigencia_contrato);
    let dataFim = new Date (itemData.data_fim_vigencia_contrato);
    let dataAtual = new Date();

    let vigente = (dataAtual >= dataIni && dataAtual <= dataFim) ? 'Sim' : 'Não';

    let html = '<a href="/fiscalizacao/contrato/' + itemData.id + '">';
    html += '<div class="alert alert-secondary" role="alert">';
    html += '<div class="row"><div class="col-11">';
    html += '<ul class="list-line">';
    html += '<li class="list-inline-item"><b>Município</b>: '+ searchConf.city +'</li>';
    html += '<li class="list-inline-item"><b>Número de Contrato</b>: '+ itemData.numero_contrato +'</li>';
    html += '<li class="list-inline-item"><b>Contratado</b>: '+ itemData.nome_negociante +'</li>';
    html += '<li class="list-inline-item"><b>Data</b>: '+ formatDate(itemData.data_contrato) +'</li>';
    html += '<li class="list-inline-item"><b>Valor total</b>: R$'+ parseFloat(itemData.valor_total_contrato).toFixed(2).toString().replace('.', ',') +'</li>';
    html += '<li class="list-inline-item"><b>CPF de gestor</b>: '+ itemData.cpf_gestor +'</li>';
    html += '<li class="list-inline-item"><b>Contrato em vigência</b>: '+ vigente +'</li>';
    html += '</ul>';
    html += '</div><div class="col-1 text-right">';
    if (itemData.alerta_nota > 0) {
        html += ' <i class="material-icons">warning</i>';
    }
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</a>';

    return html;
}

function loadPage () {
    let itemsSel = $('#items');
    itemsSel.empty();
    if (curPage >= 0 && curPage < pagesQty) {
        for (var i = 0; i < pageLimit; i++) {
            var item = listData[i + (pageLimit * curPage)];
            if (item) {
                let html = genItem(item);
                itemsSel.append(html);
            } else {
                break;
            }
        }
    }
}

function changePage (e) {
    curPage = e.target.value;
    $('html,body').scrollTop(0);
    loadPage();
}


function updateList () {
    let qty = listData.length;
    pagesQty = Math.ceil(qty/pageLimit);
    curPage = 0;

    $('#paginator').show();
    let pageSel = $('select[name="page"]');
    pageSel.find('option').remove();
    for (let i = 0; i < pagesQty; i ++) {
        pageSel.append('<option value="'+i+'">'+(parseInt(i)+1)+'</option>');
    }
    pageSel.val('0');
    loadPage (0);
    pageSel.on('change', changePage);
}

function submitForm (e) {
    e.preventDefault();
    $('#empty-msg').hide();
    $('#export-div').hide();
    $('#loading-msg').show();
    $('#paginator').hide();
    $('#items').empty();
    $('select[name="page"]').find('option').remove();

    let city = $('select[name="city"]').val();

    let begin = $('input[name="begin"]').val();
    let beginArr = begin.split('/');
    if (beginArr.length == 3) {
        begin = beginArr[2] + '-' + beginArr[1] + '-' + beginArr[0];
    }

    let end = $('input[name="end"]').val();
    let endArr = end.split('/');
    if (endArr.length == 3) {
        end = endArr[2] + '-' + endArr[1] + '-' + endArr[0];
    }

    let type = $('select[name="type"]').val();

    let min = $('input[name="min"]').val()
        .replace('R$ ', '').replace(',', '.');

    let max = $('input[name="max"]').val()
        .replace('R$ ', '').replace(',', '.');

    let fornecedor = encodeURI($('input[name="fornecedor"]').val());
    
    let url = '/api/contratos/' 
    url += city + '/' + begin + '/' + end + '/' + min + '/' + max + '/' + fornecedor;
    console.log(url);
    fetch(url)
        .then(data => {
            return data.json();
        })
        .then(res => {
            if (res) {
                listData = res;
                $('#export-div').show();
                searchConf = {
                    city: getCity(city),
                    begin: begin,
                    end: end,
                    type: type,
                    min: min,
                    max: max,
                    fornecedor: fornecedor
                };

                updateList();
            } else {
                $('#empty-msg').show();
                searchConf = {};
            }
            $('#loading-msg').hide();
        })
        .catch(err => {
            console.log('Falha ao carregar dados');
            $('#empty-msg').show();
            $('#loading-msg').hide();
            searchConf = {};       
        })
}

$(document).ready(function () {
    $('#begin').mask('00/00/0000');
    $('#end').mask('00/00/0000');
    $('#min').maskMoney({
        prefix: 'R$ ',
        thousands: '',
        decimal: ',',
        affixesStay: true,
        allowZero: true
    });
    $('#max').maskMoney({
        prefix: 'R$ ',
        thousands: '',
        decimal: ',',
        affixesStay: true,
        allowZero: true
    });
    $('#search').submit(submitForm);
});